# Welcome to the liveemily.com repository!
### This is currently a work in progress website and not functional in the slightest.
### It is not accesible yet at liveemily.com, it currently redirects my gitea instance but this will be changed in the future.

## ToDo

1. [] Default HTML mockup.
1. [] Good CSS integration.
	1. [] SASS/SCSS/LESS integration.
1. [] Maybe a little JavaScript? (This is a last resort since I'm horrible with HTML).
1. [] Possible PHP or GoLang integration.
1. [] If all goes well, might have a fully functional webserver written in C#, C++/Rust or even C.
1. [] Make it pretty for god's sake.
